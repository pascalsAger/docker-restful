import logging
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

stream_handler = logging.StreamHandler()
stream_handler.setLevel(logging.INFO)

db = SQLAlchemy()


def create_app():
    """
    Using the app factory pattern to create the application.

    Returns Flask App
    """
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object('config.settings')
    app.config.from_pyfile('settings.py', silent=True)
    db.init_app(app)    
    app.logger.addHandler(stream_handler)
    return app


if __name__=="__main__":
    create_app()