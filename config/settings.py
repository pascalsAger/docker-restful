#postgres Linux user: postcar
#postgres user: vimcar

SECRET_KEY = 'thisisasecret'

APP_NAME = 'VIMCAR'

DEBUG = True
TESTING = False

SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:vimcar@postgres:5432/vimcar'